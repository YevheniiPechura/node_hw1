const fs = require('fs').promises;
const filesExtantions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

function isValid(file) {
  const extns = file.split('.').pop();
  return filesExtantions.includes(extns);
}

const getFiles = async (req, res) => {
  try {
    const uploadedFiles = await fs.readdir('uploadedFiles');

    if (uploadedFiles.length === 0) {
      return res.status(200).json({ message: 'No uploaded files' });
    }

    res.status(200).json({ message: 'Success', files: uploadedFiles });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
};

const createFile = async (req, res) => {
  const { filename, content } = req.body;

  if (!filename || !content) {
    return res.status(400).json({
      message: 'Please specify content and filename parameters',
    });
  }

  if (!isValid(filename)) {
    return res
      .status(400)
      .json({ message: 'Provided file extension is not supported' });
  }

  try {
    const uploadedFiles = await fs.readdir('uploadedFiles');

    if (uploadedFiles.includes(filename)) {
      return res
        .status(400)
        .json({ message: `File ${filename} is already exist.` });
    }

    await fs.writeFile(`uploadedFiles/${filename}`, content);
    res.status(200).json({ message: 'File created successfully' });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
};

const deleteFile = async (req, res) => {
  const filename = req.params.filename;

  try {
    const uploadedFiles = await fs.readdir('uploadedFiles');

    if (!uploadedFiles.includes(filename)) {
      return res
        .status(400)
        .json({ message: `File with ${filename} filename wasn\`t found` });
    }

    await fs.unlink(`uploadedFiles/${filename}`);

    res.status(200).json({ message: `${filename} was successfully deleted` });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
};

const getFile = async (req, res) => {
  const filename = req.params.filename;
  try {
    const uploadedFiles = await fs.readdir('uploadedFiles');

    if (!isValid(filename)) {
      return res
        .status(400)
        .json({ message: 'Provided file extension is not supported' });
    }

    if (!uploadedFiles.includes(filename)) {
      return res
        .status(400)
        .json({ message: `File with ${filename} filename wasn\`t found` });
    }

    const content = await fs.readFile(`uploadedFiles/${filename}`, 'utf-8');
    const birthDate = (await fs.stat(`uploadedFiles/${filename}`)).birthtime;
    const extn = filename.match(/(log|txt|json|yaml|xml|js)$/g)[0];

    res
      .status(200)
      .json({ message: 'Success', filename, content, birthDate, extn });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
};

module.exports = {
  getFiles,
  createFile,
  deleteFile,
  getFile,
};
