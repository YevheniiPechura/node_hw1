const express = require('express');
const morgan = require('morgan');
const app = express();
// const PORT = process.env.PORT ?? 8080;
const PORT = 8080;
const router = require('./router');

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/', router);

app.listen(PORT, () => {
  console.log(`Server has been started on port ${PORT}`);
});
