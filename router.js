const express = require('express');
const router = express.Router();

const {
  getFiles,
  createFile,
  deleteFile,
  getFile,
} = require('./requestsMechanics');

router.get('/files', getFiles);
router.post('/files', createFile);
router.delete('/files/:filename', deleteFile);
router.get('/files/:filename', getFile);

module.exports = router;
